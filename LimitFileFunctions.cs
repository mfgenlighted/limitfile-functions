﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

//
// Version: 2.0     11/19/2013  LN
//  Added 'TestNumber' to the test configs
// Version: 3.0     9/11/2013   LN
//  Will read <FileInfo FormatVersion="x" /> in the limit file. Reading FileInfoFormatVersion
//      will return "x". If the tag is not present, "" will be returned.
// Version: 3.1     10/10/2014
//      added globals DVTMANPrompt and AppPrompt
namespace LimitFileFunctions
{

    //-----------------------------
    // Tests to run
    public class ListEntry
    {
        public string Parameter { get { return lclParameter; } }
        public string Type { get { return lclType; } }
        public string Operation { get { return lclOperation; } }
        public string Value1 { get { return lclValue1; } }
        public string Value2 { get { return lclValue2; } }
        public string Options { get { return lclOptions; } }

        private string lclParameter;
        private string lclType;
        private string lclOperation;
        private string lclValue1;
        private string lclValue2;
        private string lclOptions;


        public ListEntry(string parameter, string type, string operation, string value, string options)
        {
            lclParameter = parameter;
            lclType = type;
            lclOperation = operation;
            lclOptions = options;
            if (type.Contains("Range"))
            {
                lclValue1 = value.Substring(0, value.IndexOf(','));
                lclValue2 = value.Substring(value.IndexOf(',') + 1);
            }
            else
            {
                lclValue1 = value;
                lclValue2 = string.Empty;
            }

        }

    }

    public class ConstantEntry
    {
        public string Name { get { return lclname; } }
        public string Value { get { return lclvalue; } }

        private string lclname;
        private string lclvalue;

        public ConstantEntry(string name, string value)
        {
            lclname = name;
            lclvalue = value;
        }
    }

    public class DllFunctionEntry
    {
        public string FileName { get { return lclFileName; } }
        public string ClassName { get { return lclClassName; } }
        public string FunctionName { get { return lclFunctionName; } }

        private string lclFileName;
        private string lclClassName;
        private string lclFunctionName;

        public DllFunctionEntry(string name, string className, string function)
        {
            lclFileName = name;
            lclFunctionName = function;
            lclClassName = className;
        }
    }

    public class TestToRun
    {
        private string lclLowLimit;
        private string lclHighLimit;
        private string lclOperation;
        private string lclLimitType;
        private string lclName;
        private string lclFunction;
        private string lclTestType;
        private int lclTestNumber;
        private List<ListEntry> lclPList;
        private List<ConstantEntry> lclCList;
        private string lclDLLFileName;
        private string lclDLLClassName;
        private string lclRunTestOption;
        private string lclFailTestOption;
        private string lclPassTestOption;

        public string LowLimit          { get { return lclLowLimit; } }
        public string HighLimit         { get { return lclHighLimit; } }
        public string Operation         { get { return lclOperation; } }
        public string LimitType         { get { return lclLimitType; } }
        public string Name              { get { return lclName; } }
        public string Function          { get { return lclFunction; } }
        public string TestType          { get { return lclTestType; } }
        public int TestNumber           { set { lclTestNumber = value; } get { return lclTestNumber; } }
        public List<ListEntry> PList    { get { return lclPList; } }
        public List<ConstantEntry> CList { get { return lclCList; } }
        public string DLLFileName       {get {return lclDLLFileName; } }
        public string DLLClassName      {get {return lclDLLClassName; } }
        public string RunTestOption { get { return lclRunTestOption; } }
        public string FailTestOption    { get { return lclFailTestOption; } }
        public string PassTestOption    { get { return lclPassTestOption; } }

        public TestToRun()
        {
            lclName = string.Empty;
            lclTestNumber = 0;
            lclFunction = string.Empty;
            lclDLLFileName = string.Empty;
            lclDLLClassName = string.Empty;
            lclTestType = string.Empty;
            lclLimitType = string.Empty;
            lclLowLimit = string.Empty;
            lclHighLimit = string.Empty;
            lclOperation = string.Empty;
            lclPList = new List<ListEntry>();
            lclCList = new List<ConstantEntry>();
            lclRunTestOption = "SystemConfig";
            lclFailTestOption = "SystemConfig";
            lclPassTestOption = "SystemConfig";
        }

        public TestToRun(string name, int testnumber, string function, string dllFile, string dllClass, string testType, string lLimit, string hLimit, string op, string limittype)
        {
            lclName = name;
            lclTestNumber = testnumber;
            lclFunction = function;
            lclDLLFileName = dllFile;
            lclDLLClassName = dllClass;
            lclTestType = testType;
            lclLowLimit = lLimit;
            lclHighLimit = hLimit;
            lclOperation = op;
            lclLimitType = limittype;
            lclCList = new List<ConstantEntry>();
            lclRunTestOption = "SystemConfig";
            lclFailTestOption = "SystemConfig";
            lclPassTestOption = "SystemConfig";
        }
        public TestToRun(string name, int testnumber, string function, string dllFile, string dllClass, string testType, string limit, string op, string limittype)
        {
            lclName = name;
            lclTestNumber = testnumber;
            lclFunction = function;
            lclDLLFileName = dllFile;
            lclDLLClassName = dllClass;
            lclTestType = testType;
            lclLowLimit = limit;
            lclHighLimit = string.Empty;
            lclOperation = op;
            lclLimitType = limittype;
            lclCList = new List<ConstantEntry>();
            lclRunTestOption = "SystemConfig";
            lclFailTestOption = "SystemConfig";
            lclPassTestOption = "SystemConfig";
        }

        public TestToRun(string name, int testnumber, string function, string dllFile, string dllClass, string testType, string limittype)
        {
            lclName = name;
            lclTestNumber = testnumber;
            lclFunction = function;
            lclDLLFileName = dllFile;
            lclDLLClassName = dllClass;
            lclTestType = testType;
            lclLimitType = limittype;
            lclLowLimit = string.Empty;
            lclHighLimit = string.Empty;
            lclOperation = string.Empty;
            lclPList = new List<ListEntry>();
            lclCList = new List<ConstantEntry>();
            lclRunTestOption = "SystemConfig";
            lclFailTestOption = "SystemConfig";
            lclPassTestOption = "SystemConfig";
        }

        //public void AddParam(string param, string type, string operation, string value)
        //{
        //    ListEntry list = new ListEntry(param, type, operation, value, "");
        //    lclPList.Add(list);
        //}

        public void AddParam(string param, string type, string operation, string value, string options)
        {
            ListEntry list = new ListEntry(param, type, operation, value, options);
            lclPList.Add(list);
        }

        public void AddConstant(string param, string value)
        {
            ConstantEntry list = new ConstantEntry(param, value);
            lclCList.Add(list);
        }

        public void AddOnPass(string value)
        {
            lclPassTestOption = value;
        }

        public void AddOnFail(string value)
        {
            lclFailTestOption = value;
        }
        public void AddRunTestOption(string value)
        {
            lclRunTestOption = value;
        }
    }

    public class LimitFileFunctions
    {

        //-----------------
        // Test file values
        private string fileInfoFormatVersion = string.Empty;
        public string FileInfoFormatVersion { get { return fileInfoFormatVersion; } }

        // tests to run before the main body
        private List<TestToRun> preTests = new List<TestToRun>();
        public List<TestToRun> PreTests { get { return preTests; } }

        // main tests to run
        private List<TestToRun> testsToRun = new List<TestToRun>();
        public List<TestToRun> TestsToRun { get { return testsToRun; } }

        // test to run after main body
        private List<TestToRun> postTests = new List<TestToRun>();
        public List<TestToRun> PostTests { get { return postTests; } }

        private int currentTestIndex = 0;
        //private string macBarcodeFileName = string.Empty;
        //public string MacBarcodeFileName { get { return macBarcodeFileName; } }
        //private string uutPrompt = string.Empty;                // keep for backwards compatiable. Use dvtmanPrompt and appPrompt for new
        //public string UUTPrompt { get { return uutPrompt; } }
        private string dvtmanPrompt = "DVTMAN> ";                       // default to DVTMAN>
        public string DVTMANPrompt { get { return dvtmanPrompt; } }
        private string appPrompt = "SU> ";
        public string AppPrompt { get { return appPrompt; } }
        private bool printMACLabel = true;
        public bool PrintMACLabel { get { return printMACLabel; } }
        private string lcllimitFileVersion;
        public string LimitFileVersion { get { return lcllimitFileVersion; } }
        private string lcllimitFileName;
        public string LimitFileName { get { return lcllimitFileName; } }


        //---------------------------------------
        // Read the test limit file
        //
        // Will read the test limit file and create a test list.
        //
        //  Limit file format:
        //      <Limts>
        //          <Version>file version</Version>
        //          <product code>|<ALL>
        //              <Test>
        //                  <Name> test name to display </Name>
        //                  <Function> function to call </Function>
        //                  <TestType> type of test to run </TestType>
        //                  limit data
        //                  constant data
        //              </Test>
        //              ...
        //          </product code>|<ALL>
        //      </Limts>
        //
        //  Limit data
        //	    High/Low data
        //		<LLimit> value </LLimit>
        //		<LLimit> value </LLimit>
        //        <Operation>  HLoperation </Operation>
        //    Single data
        //        <Limit> value </Limit> 
        //        <Operation> Soperation </Operation>
        //    String data
        //        <String> string </String>
        //        <Operation> Soperation </Operation>
        //  Constant data
        //        <TestConstant param="value name">value</TestConstant>

        public bool ReadTestLimitFile(string productcode, string limitFileName, ref string errormsg)
        {
            string lowLimit = string.Empty;
            string highLimit = string.Empty;
            string operation = string.Empty;
            string limitType = string.Empty;
            string name = string.Empty;
            string function = string.Empty;
            string dllFile = string.Empty;
            string dllClass = string.Empty;
            string testType = string.Empty;
            string param = string.Empty;
            string paramType = string.Empty;
            string paramValue1 = string.Empty;
            string paramValue2 = string.Empty;
            string paramOptions = string.Empty;
            XDocument configdoc = null;

            errormsg = string.Empty;

            try     // read the file
            {
                configdoc = XDocument.Load(limitFileName);
            }
            catch (Exception e)
            {
                errormsg = "Problem opening the limit file " + limitFileName + "\n" + e.Message;
                return false;
            }
            if (configdoc.Element("Limits").Element("Version") != null)
            {
                lcllimitFileVersion = configdoc.Element("Limits").Element("Version").Value;
                lcllimitFileName = limitFileName;
            }
            else
            {
                errormsg = "Missing the Version tag in the limit file " + limitFileName + "\n";
                return false;
            }

            if (configdoc.Element("Limits").Element("FileInfo") != null)
                fileInfoFormatVersion = configdoc.Element("Limits").Element("FileInfo").Attribute("FormatVersion").Value;

                // Verify that this product code is valid
            if (configdoc.Element("Limits").Element(productcode) == null)
            {
                errormsg = "Product code " + productcode + " is not in the limit file " + limitFileName;
                return false;
            }

                // Get the Global Test info
            if (configdoc.Element("Limits").Element(productcode).Element("DVTMANPrompt") != null)
                dvtmanPrompt = configdoc.Element("Limits").Element(productcode).Element("DVTMANPrompt").Value;

            if (configdoc.Element("Limits").Element(productcode).Element("AppPrompt") != null)
                appPrompt = configdoc.Element("Limits").Element(productcode).Element("AppPrompt").Value;

            if (configdoc.Element("Limits").Element(productcode).Element("PrintMACLabel") != null)
            {
                if (configdoc.Element("Limits").Element(productcode).Element("PrintMACLabel").Value.Equals("YES", StringComparison.OrdinalIgnoreCase))
                    printMACLabel = true;
                else if (configdoc.Element("Limits").Element(productcode).Element("PrintMACLabel").Value.Equals("NO", StringComparison.OrdinalIgnoreCase))
                    printMACLabel = false;
                else
                {
                    errormsg = string.Format("Test limits error: {0} is not a valid parameter for PrintMACLabel. " +
                        "Only 'YES' and 'NO' are valid.", configdoc.Element("Limits").Element(productcode).Element("PrintMACLabel").Value);
                    return false;
                }
            }

                // Get the Test Step info
            testsToRun.Clear();
            preTests.Clear();
            postTests.Clear();

            getSectionData(configdoc, productcode, "PreTest", ref preTests, ref errormsg);
            getSectionData(configdoc, productcode, "Test", ref testsToRun, ref errormsg);
            getSectionData(configdoc, productcode, "PostTest", ref postTests, ref errormsg);

            if (errormsg == string.Empty)
                return true;
            else
                return false;
        }


        //-----------------------------
        // GetFirstTestLimits
        //
        public bool GetFirstTestLimits(ref string testname, ref string function, ref string testtype, ref string lowlimit,
            ref string hightlimit, ref string operation, ref string limittype)
        {
            TestToRun tl = null;

            tl = testsToRun.First();
            testname = tl.Name;
            lowlimit = tl.LowLimit;
            hightlimit = tl.HighLimit;
            operation = tl.Operation;
            limittype = tl.LimitType;
            testtype = tl.TestType;
            function = tl.Function;

            currentTestIndex = 0;

            return true;
        }

        //-----------------------------
        // GetNextTestLimits
        //
        public bool GetNextTestLimits(ref string testname, ref string function, ref string testtype, ref string lowlimit,
            ref string hightlimit, ref string operation, ref string limittype)
        {
            TestToRun tl = null;

            try
            {
                tl = testsToRun.ElementAt(++currentTestIndex);
                testname = tl.Name;
                lowlimit = tl.LowLimit;
                hightlimit = tl.HighLimit;
                operation = tl.Operation;
                limittype = tl.LimitType;
                testtype = tl.TestType;
                function = tl.Function;

                currentTestIndex++;

            }
            catch (ArgumentOutOfRangeException)
            {
                return false;
            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }

        //-----------------------------
        // GetTestLimits
        //
        public bool GetTestLimits(int index, ref string testname, ref string function, ref string testtype, ref string lowlimit,
            ref string hightlimit, ref string operation, ref string limittype)
        {
            TestToRun tl = null;

            try
            {
                tl = testsToRun.ElementAt(index);
                testname = tl.Name;
                lowlimit = tl.LowLimit;
                hightlimit = tl.HighLimit;
                operation = tl.Operation;
                limittype = tl.LimitType;
                testtype = tl.TestType;
                function = tl.Function;

                currentTestIndex = index;

            }
            catch (ArgumentOutOfRangeException)
            {
                return false;
            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }

        //-----------------------------
        // GetTestLimitsByINdex
        /// <summary>
        /// Get test params from the Limit File by the positon of the test in the limit file. ReadTestLimitFile must be called
        /// before to read the limit file.
        /// </summary>
        /// <param name="testname">int - postion of the test in the file. Zero based.</param>
        /// <param name="testValues">TestToRun - test params for the given test. Will be null if name was not found.</param>
        /// <returns>true - name was found and data returned</returns>
        /// <exception cref="System.Exception">Thrown if limit list has not been loaded</exception>
        public bool GetTestLimitsByTestIndex(int index, ref TestToRun testValues)
        {

            if (testsToRun.Count == 0)              // if there are no tests read
            {
                throw new Exception("Error: Limit file has not been loaded. Call ReadTestLimitFile() to load.");
            }


            if (index < testsToRun.Count)      // if the test position is with the test count
            {
                testValues = testsToRun.ElementAt(index);
            }
            else    // test position is out of range
            {
                testValues = null;
                return false;
            }
            return true;
        }

        //-----------------------------
        // GetTestLimitsByTestName
        /// <summary>
        /// Get test params from the Limit File by the test name. ReadTestLimitFile must be called
        /// before to read the limit file.
        /// </summary>
        /// <param name="testname">string - Name of the test to look for. Name tag in the list.</param>
        /// <param name="testValues">TestToRun - test params for the given test. Will be null if name was not found.</param>
        /// <returns>true - name was found and data returned</returns>
        /// <exception cref="System.Exception">Thrown if limit list has not been loaded</exception>
        public bool GetTestLimitsByTestName(string testname, ref TestToRun testValues)
        {
            int index = 0;
            bool found = false;

            if (testsToRun.Count == 0)              // if there are no tests read
            {
                throw new Exception("Error: Limit file has not been loaded. Call ReadTestLimitFile() to load.");
            }

            // look for a test with the same name
            while (!found && (index < testsToRun.Count))
            {
                if (testname.Equals(testsToRun.ElementAt(index).Name, StringComparison.CurrentCultureIgnoreCase))
                    found = true;
                else
                    index++;
            }

            if (found)
            {
                testValues = testsToRun.ElementAt(index);
            }
            else    // name was not found
            {
                testValues = null;
                return false;
            }
            return true;
        }

        //-----------------------------
        // GetTestLimitsByTestFunction
        /// <summary>
        /// Get test params from the Limit File by the function. ReadTestLimitFile must be called
        /// before to read the limit file.
        /// </summary>
        /// <param name="testname">string - function of the test to look for. Name tag in the list.</param>
        /// <param name="testValues">TestToRun - test params for the given test. Will be null if name was not found.</param>
        /// <returns>true - function was found and data returned</returns>
        /// <exception cref="System.Exception">Thrown if limit list has not been loaded</exception>
        public bool GetTestLimitsByTestFunction(string function, ref TestToRun testValues)
        {
            int index = 0;
            bool found = false;

            if (testsToRun.Count == 0)              // if there are no tests read
            {
                throw new Exception("Error: Limit file has not been loaded. Call ReadTestLimitFile() to load.");
            }

            // look for a test with the same name
            while (!found && (index < testsToRun.Count))
            {
                if (function.Equals(testsToRun.ElementAt(index).Function, StringComparison.CurrentCultureIgnoreCase))
                    found = true;
                else
                    index++;
            }

            if (found)
            {
                testValues = testsToRun.ElementAt(index);
            }
            else    // name was not found
            {
                testValues = null;
                return false;
            }
            return true;
        }

        public bool GetCurrentTestLimitStuct(ref TestToRun test)
        {
            try
            {
                test = testsToRun.ElementAt(currentTestIndex);
            }
            catch (ArgumentOutOfRangeException)
            {
                return false;
            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }

        public bool GetIndexedTestLimitStuct(int index, ref TestToRun test)
        {
            try
            {
                test = testsToRun.ElementAt(index);
            }
            catch (ArgumentOutOfRangeException)
            {
                return false;
            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Will get the data for a given constant in the CList. Returns empty string if not found.
        /// </summary>
        /// <param name="testData">structure that has the CList</param>
        /// <param name="constantName">name of the constant to get data for</param>
        /// <param name="constData">returned data. string.empty if none found</param>
        /// <returns>true = name is found</returns>
        public bool GetConstantValue(TestToRun testData, string constantName, ref string constValue)
        {
            bool tagfound = false;

            constValue = string.Empty;
            foreach (var item in testData.CList)   // go through the parameters in the test
            {
                if (item.Name.ToUpper().Equals(constantName.ToUpper()))
                {
                    constValue = item.Value;
                    tagfound = true;
                }
            }
            return tagfound;
        }


        // utils
        /// <summary>
        /// Will get a section of the limit file.
        /// </summary>
        /// <param name="configdoc">The limit file XDoc</param>
        /// <param name="productcode">Product code within the limit file</param>
        /// <param name="section">Test, PreTest, PostTest section to read</param>
        /// <param name="testList">returned section</param>
        /// <param name="errormsg"></param>
        /// <returns>0 = no problems</returns>
        private int getSectionData(XDocument configdoc, string productcode, string section,
            ref List<TestToRun> testList, ref string errormsg)
        {
            string lowLimit = string.Empty;
            string highLimit = string.Empty;
            string operation = string.Empty;
            string limitType = string.Empty;
            string name = string.Empty;
            int testnumber = 0;
            string function = string.Empty;
            string dllFile = string.Empty;
            string dllClass = string.Empty;
            string testType = string.Empty;
            string param = string.Empty;
            string paramType = string.Empty;
            string paramValue1 = string.Empty;
            string paramValue2 = string.Empty;
            string paramOptions = string.Empty;

            TestToRun newtest = new TestToRun();

            int stepNumber = 1;

            errormsg = string.Empty;

            foreach (var step in configdoc.Descendants(productcode).Descendants(section))
            {
                newtest = null;                     // start new step def
                if (step.Element("Name") == null)
                {
                    errormsg = "Missing Name tag in step " + stepNumber.ToString();
                    break;
                }
                name = step.Element("Name").Value;

                // test number is optional. the code has the test numbers hardcoded. If
                //  the test number is present, it will overwrite the hardcoded.
                if (step.Element("TestNumber") == null)
                    testnumber = 0;
                else
                    testnumber = Convert.ToInt32(step.Element("TestNumber").Value);
                if (step.Element("Function") == null)
                {
                    errormsg = "Missing Function tag in step " + stepNumber.ToString();
                    break;
                }
                function = step.Element("Function").Value;
                if (step.Element("TestType") == null)
                {
                    errormsg = "Missing TestType tag in step " + stepNumber.ToString();
                    break;
                }
                testType = step.Element("TestType").Value;
                if (step.Element("DLL") != null)
                {
                    dllFile = step.Element("DLL").Attribute("File").Value;
                    dllClass = step.Element("DLL").Attribute("Class").Value;
                }

                    // -----process Limit types
                    // string compare test
                if (testType.ToUpper().Equals("STRING")) 
                {
                    if (step.Element("LLimit") == null)
                    {
                        errormsg = "Missing LLimit tag in step " + stepNumber.ToString();
                        break;
                    }
                    lowLimit = step.Element("LLimit").Value;
                    highLimit = string.Empty;
                    if (step.Element("Operation") == null)
                    {
                        errormsg = "Missing Operation tag in step " + stepNumber.ToString();
                        break;
                    }
                    operation = step.Element("Operation").Value;
                    newtest = new TestToRun(name, testnumber, function, dllFile, dllClass, testType, lowLimit, highLimit, operation, limitType);
                }

                    // high/low compare test
                else if (testType.ToUpper().Equals("HIGHLOWLIMIT"))
                {
                    if (step.Element("LLimit") == null)
                    {
                        errormsg = "Missing LLimit tag in step " + stepNumber.ToString();
                        break;
                    }
                    lowLimit = step.Element("LLimit").Value;
                    if (step.Element("HLimit") == null)
                    {
                        errormsg = "Missing HLimit tag in step " + stepNumber.ToString();
                        break;
                    }
                    highLimit = step.Element("HLimit").Value;
                    if (step.Element("Operation") == null)
                    {
                        errormsg = "Missing Operation tag in step " + stepNumber.ToString();
                        break;
                    }
                    operation = step.Element("Operation").Value;
                    newtest = new TestToRun(name, testnumber, function, dllFile, dllClass, testType, lowLimit, highLimit, operation, limitType);
                }

                    // Limit type
                else if (testType.ToUpper().Equals("LIST"))
                {
                    newtest = new TestToRun(name, testnumber, function, dllFile, dllClass, testType, "List");
                    foreach (var llstep in step.Descendants("LimitList"))
                    {
                        foreach (var lstep in llstep.Descendants("Limit"))
                        {
                            param = lstep.Attribute("param").Value;
                            paramType = lstep.Attribute("type").Value;
                            operation = lstep.Attribute("operation").Value;
                            lowLimit = lstep.Value;
                            if (lstep.Attribute("options") != null)
                                paramOptions = lstep.Attribute("options").Value;
                            newtest.AddParam(param, paramType, operation, lowLimit, paramOptions);
                        }
                    }
                }

                        // Run or PassFail. takes no paramerters
                else if (testType.ToUpper().Equals("RUN") | testType.ToUpper().Equals("PASSFAIL"))
                {
                    newtest = new TestToRun(name, testnumber, function, dllFile, dllClass, testType, "");
                }
                    
                    // invalid test type
                else
                {
                    errormsg = "Invalid testType(" + testType + ") tag in step " + stepNumber.ToString();
                    break;
                }
                    // ---- Finish processing Limit types

                if (step.Element("TestConstant") != null)
                {
                    if (newtest == null)
                    {
                        errormsg = "TestConstant defined with no valid tests in step " + stepNumber.ToString();
                        break;
                    }
                    foreach (var cstep in step.Elements("TestConstant"))
                    {
                        lowLimit = cstep.Value;
                        param = cstep.Attribute("param").Value;
                        newtest.AddConstant(param, lowLimit);
                    }
                }

                // Process the NextOn statments
                if (step.Element("OnFail") != null)
                {
                    if (newtest == null)
                    {
                        errormsg = "OnFail defined with no valid tests in step " + stepNumber.ToString();
                        break;
                    }
                    newtest.AddOnFail(step.Element("OnFail").Value);
                }
                if (step.Element("OnPass") != null)
                {
                    if (newtest == null)
                    {
                        errormsg = "OnPass defined with no valid tests in step " + stepNumber.ToString();
                        break;
                    }
                    newtest.AddOnPass(step.Element("OnPass").Value);
                }

                // Process the RunOption statment
                if (step.Element("RunTestOption") != null)
                    newtest.AddRunTestOption(step.Element("RunTestOption").Value);

                testList.Add(newtest);
                stepNumber++;
            }
            if (errormsg != string.Empty)
                return -1;
            else
                return 0;
        }


    }

}
